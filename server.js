var express = require('express');
var nodemailer = require("nodemailer");
var bodyParser = require('body-parser')
var ejs = require("ejs");
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
// var app = require('express')(),
    // mailer = require('express-mailer');

var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // use SSL
        auth: {
            user: 'kcode.dev@gmail.com',
            pass: 'numlock123'
          }
    });
// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/dist'));

// set the home page route
app.get('/', function(req, res) {

    // ejs render automatically looks in the views folder
    res.render('index');
});

app.post('/send', function (req, res) {
    console.log(req.body)
    ejs.renderFile(__dirname + "/views/mail.ejs", req.body, function (err, data) {
        if (err) {
            console.log(err);
        } else {
            var mainOptions = {
                from: '"LeadTapir" no-reply@leadtapir.com',
                to: "fai@boxgreen.co",
                subject: 'LeadTapir Enquiry',
                html: data
            };
            transporter.sendMail(mainOptions, function (err, info) {
                if (err) {
                    console.log(err);
                    res.status(500).send(err)
                } else {
                    console.log('Message sent: ' + info.response);
                    res.sendStatus(200)
                }
            });
        }
        
        });
});

app.listen(port, function() {
    console.log('Our app is running on http://localhost:' + port);
});