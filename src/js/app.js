// Load Popper.js
window.Popper = require('popper.js/dist/umd/popper');

// Load Bootstrap.js
try {
    window.$ = window.jQuery = require('jquery/dist/jquery');

    require('bootstrap');
} catch (e) {}