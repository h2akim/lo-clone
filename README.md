Installation

### Clone repo
```
git clone git@gitlab.com:h2akim/lo-clone.git
```

### Install dependencies (to rebuild)
```
npm install
```